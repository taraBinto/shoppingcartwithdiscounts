import CartClasses.BaseOrder;
import CartClasses.LineItem;
import org.junit.jupiter.api.Test;

public class BaseOrderTest {

    private final String SHIRT_LABEL = "Shirt";
    private final String TROUSER_LABEL = "Trouser";

    @Test
    public void testCreation1Item(){

        BaseOrder baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 1, 10);
        baseOrder.addItem( shirt);

        assert(baseOrder.getPrice() == 10);
        assert(baseOrder.getLineItems().size() == 1);

        assert(baseOrder.getLineItems().get(0).getItemName() == SHIRT_LABEL);
        assert(baseOrder.getLineItems().get(0).getPrice() == 10);
        assert(baseOrder.getLineItems().get(0).getQuantity() == 1);
        assert(baseOrder.getLineItems().get(0).getLinePrice() == 10);
    }

    @Test
    public void testCreation4Items(){

        BaseOrder baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 4, 10);
        baseOrder.addItem( shirt);

        assert(baseOrder.getPrice() == 40);
        assert(baseOrder.getLineItems().size() == 1);

        assert(baseOrder.getLineItems().get(0).getItemName() == SHIRT_LABEL);
        assert(baseOrder.getLineItems().get(0).getPrice() == 10);
        assert(baseOrder.getLineItems().get(0).getQuantity() == 4);
        assert(baseOrder.getLineItems().get(0).getLinePrice() == 40);
    }


    @Test
    public void testCreation2LineItems(){

        BaseOrder baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 1, 10);
        LineItem trouser = new LineItem(TROUSER_LABEL, 1, 50);

        baseOrder.addItem( shirt);
        baseOrder.addItem( trouser);

        assert(baseOrder.getPrice() == 60);
        assert(baseOrder.getLineItems().size() == 2);

        assert(baseOrder.getLineItems().get(0).getItemName() == SHIRT_LABEL);
        assert(baseOrder.getLineItems().get(0).getPrice() == 10);
        assert(baseOrder.getLineItems().get(0).getQuantity() == 1);
        assert(baseOrder.getLineItems().get(0).getLinePrice() == 10);

        assert(baseOrder.getLineItems().get(1).getItemName() == TROUSER_LABEL);
        assert(baseOrder.getLineItems().get(1).getPrice() == 50);
        assert(baseOrder.getLineItems().get(1).getQuantity() == 1);
        assert(baseOrder.getLineItems().get(1).getLinePrice() == 50);
    }

    @Test
    public void testCreation4LineItems(){

        BaseOrder baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 2, 10);
        LineItem trouser = new LineItem(TROUSER_LABEL, 2, 50);

        baseOrder.addItem( shirt);
        baseOrder.addItem( trouser);

        assert(baseOrder.getPrice() == 120);
        assert(baseOrder.getLineItems().size() == 2);

        assert(baseOrder.getLineItems().get(0).getItemName() == SHIRT_LABEL);
        assert(baseOrder.getLineItems().get(0).getPrice() == 10);
        assert(baseOrder.getLineItems().get(0).getQuantity() == 2);
        assert(baseOrder.getLineItems().get(0).getLinePrice() == 20);

        assert(baseOrder.getLineItems().get(1).getItemName() == TROUSER_LABEL);
        assert(baseOrder.getLineItems().get(1).getPrice() == 50);
        assert(baseOrder.getLineItems().get(1).getQuantity() == 2);
        assert(baseOrder.getLineItems().get(1).getLinePrice() == 100);
    }
}
