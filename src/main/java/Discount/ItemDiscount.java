package Discount;

import CartClasses.BaseOrder;
import CartClasses.LineItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ItemDiscount implements Order{

    private Order baseOrder;

    private String itemToDiscount;
    private double discountPercentage;

    public ItemDiscount( Order baseOrder, String itemToDiscount, double discountPercentage) {
        this.baseOrder = baseOrder;
        this.itemToDiscount = itemToDiscount;
        this.discountPercentage = discountPercentage / 100;
    }

    public void addItem(LineItem item) {
        baseOrder.addItem(item);
    }

    public List<LineItem> getLineItems() {
        return baseOrder.getLineItems();
    }

    public int getNumberOfItems() {
        return baseOrder.getNumberOfItems();
    }

    public double getPrice() {
        double ammountToDiscount = 0;

        for( LineItem lineItem: baseOrder.getLineItems()){

            if( lineItem.getItemName().equals(itemToDiscount)){
                ammountToDiscount += lineItem.getLinePrice() * discountPercentage;
            }
        }
        return baseOrder.getPrice() - ammountToDiscount;
    }

}
