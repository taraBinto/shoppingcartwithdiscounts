import CartClasses.LineItem;
import org.junit.jupiter.api.Test;

public class LineItemTest {

    private final String SHIRT_LABEL = "Shirt";
    private final String TROUSER_LABEL = "Trouser";

    @Test
    public void testCreation1Item(){

        LineItem shirt = new LineItem(SHIRT_LABEL, 1, 10);

        assert(shirt.getItemName() == SHIRT_LABEL);
        assert(shirt.getPrice() == 10);
        assert(shirt.getQuantity() == 1);
        assert(shirt.getLinePrice() == 10);

    }

    @Test
    public void testCreation4Items(){

        LineItem shirt = new LineItem(SHIRT_LABEL, 4, 10);

        assert(shirt.getItemName() == SHIRT_LABEL);
        assert(shirt.getPrice() == 10);
        assert(shirt.getQuantity() == 4);
        assert(shirt.getLinePrice() == 40);

    }


}
