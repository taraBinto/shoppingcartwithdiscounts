package Discount;

import CartClasses.BaseOrder;
import CartClasses.LineItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExtraItemFree implements Order{

    private Order baseOrder;

    private String itemToDiscount;

    public ExtraItemFree(Order baseOrder, String itemToDiscount) {
        this.baseOrder = baseOrder;
        this.itemToDiscount = itemToDiscount;
    }


    public void addItem(LineItem item) {
        baseOrder.addItem(item);
    }

    public List<LineItem> getLineItems() {

        List<LineItem> allLineItems = new ArrayList<LineItem>();

        allLineItems.addAll(baseOrder.getLineItems());
        allLineItems.addAll(getExtraItems());

        return allLineItems;
    }

    public int getNumberOfItems() {
        return baseOrder.getNumberOfItems() + getNumberOfExtraItems();
    }

    public double getPrice() {
        return baseOrder.getPrice();
    }

    private List<LineItem> getExtraItems() {

        List<LineItem> orderItems = baseOrder.getLineItems();
        List<LineItem> extraItems = new ArrayList<LineItem>();

        for( LineItem lineItem: orderItems){

            if( lineItem.getItemName().equals(itemToDiscount)){

                extraItems.add(new LineItem (lineItem.getItemName(), lineItem.getQuantity(), 0));
            }
        }

        return extraItems;
    }

    private int getNumberOfExtraItems() {

        List<LineItem> orderItems = baseOrder.getLineItems();
        int extraItems = 0;

        for( LineItem lineItem: orderItems){

            if( lineItem.getItemName().equals(itemToDiscount)){
                extraItems += lineItem.getQuantity();
            }
        }

        return extraItems;
    }
}
