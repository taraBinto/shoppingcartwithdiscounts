package Discount;

import CartClasses.LineItem;

import java.util.List;

public interface Order {

    public void addItem(LineItem item);
    public List<LineItem> getLineItems();
    public int getNumberOfItems();
    public double getPrice();
}
