import CartClasses.BaseOrder;
import CartClasses.LineItem;
import Discount.ExtraItemFree;
import Discount.ItemDiscount;
import Discount.Order;
import org.junit.jupiter.api.Test;

public class ShoppingCartTestStackDiscounts {

    private final String SHIRT_LABEL = "Shirt";
    private final String TROUSER_LABEL = "Trouser";

    @Test
    public void testDiscount2Items2Discounts(){

        Order baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 1, 10);
        LineItem trouser = new LineItem(TROUSER_LABEL, 1, 50);
        baseOrder = new ItemDiscount( baseOrder, SHIRT_LABEL, 50);
        baseOrder = new ExtraItemFree( baseOrder, SHIRT_LABEL);

        baseOrder.addItem( shirt);
        baseOrder.addItem( trouser);

        assert(baseOrder.getPrice() == 55);
        assert(baseOrder.getLineItems().size() == 3);
        assert(baseOrder.getNumberOfItems() == 3);
    }

    @Test
    public void testDiscount2Items2LineItems(){

        Order baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 2, 10);
        LineItem trouser = new LineItem(TROUSER_LABEL, 1, 50);
        baseOrder = new ItemDiscount( baseOrder, SHIRT_LABEL, 50);
        baseOrder = new ExtraItemFree( baseOrder, SHIRT_LABEL);

        baseOrder.addItem( shirt);
        baseOrder.addItem( trouser);

        assert(baseOrder.getPrice() == 60);
        assert(baseOrder.getLineItems().size() == 3);
        assert(baseOrder.getNumberOfItems() == 5);
    }

    @Test
    public void testDiscount3Items4LineItems(){

        Order baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 2, 10);
        LineItem trouser = new LineItem(TROUSER_LABEL, 4, 50);
        baseOrder = new ItemDiscount( baseOrder, SHIRT_LABEL, 50);
        baseOrder = new ExtraItemFree( baseOrder, SHIRT_LABEL);

        baseOrder.addItem( shirt);
        baseOrder.addItem( trouser);
        baseOrder.addItem( shirt);

        assert(baseOrder.getPrice() == 220);
        assert(baseOrder.getLineItems().size() == 5);
        assert(baseOrder.getNumberOfItems() == 12);
    }
}
