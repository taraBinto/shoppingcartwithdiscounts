import CartClasses.BaseOrder;
import CartClasses.LineItem;
import org.junit.jupiter.api.Test;

public class ShoppingCartTestNoDiscount {

    private final String SHIRT_LABEL = "Shirt";
    private final String TROUSER_LABEL = "Trouser";

    @Test
    public void testNoDiscount(){

        BaseOrder baseOrder = new BaseOrder();
        LineItem shirt = new LineItem(SHIRT_LABEL, 1, 10);

        baseOrder.addItem( shirt);

        assert(baseOrder.getPrice() == 10);
        assert(baseOrder.getNumberOfItems() == 1);
    }
}
