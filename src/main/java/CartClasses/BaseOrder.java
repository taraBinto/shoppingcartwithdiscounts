package CartClasses;

import Discount.Order;

import java.util.ArrayList;
import java.util.List;

public class BaseOrder implements Order {

    List<LineItem> items = new ArrayList<LineItem>();

    public void addItem(LineItem item) {
        items.add(item);
    }

    public List<LineItem> getLineItems() {
        return items;
    }

    public int getNumberOfItems() {

        int numberOfItems = 0;

        for (LineItem item : items) {

            numberOfItems += item.getQuantity();
        }
        return numberOfItems;
    }

    public double getPrice() {
        double totalPriceOrder = 0;

        for (LineItem item : items) {

            totalPriceOrder += item.getLinePrice();
        }
        return totalPriceOrder;
    }

}
